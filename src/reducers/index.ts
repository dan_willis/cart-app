import { CartAction } from '../actions';
import { StoreState } from '../types';
import { ADD_PRODUCT_TO_CART, REMOVE_PRODUCT_FROM_CART } from '../constants';
import { CartItem, Product } from '../models/Product';

export const initialProducts = [
  {
    name: 'Sledgehammer',
    price: 125.76
  }, {
    name: 'Axe',
    price: 190.51
  }, {
    name: 'Bandsaw',
    price: 562.14
  },{
    name: 'Chisel',
    price: 13.9
  },{
    name: 'Hacksaw',
    price: 19.45
  }
];

export const initialState: StoreState = {
  productList: initialProducts,
  cart: [],
};

export function cartReducer(state: StoreState = initialState, action: CartAction): StoreState  {
  switch (action.type) {
    case ADD_PRODUCT_TO_CART:
      return { ...state, cart: doAddProduct(state.productList, state.cart, action.payload) };
    case REMOVE_PRODUCT_FROM_CART:
      return { ...state, cart: doRemoveProduct(state.productList, state.cart, action.payload) };
    default:
      return state;
  }
}

function doAddProduct(allProducts: Product[], cart: CartItem[], productName: string ): CartItem[] {
  const productInCart = cart.find((cartItem: CartItem) => cartItem.productName === productName);

  // If product is already in cart, increment quantity
  if (!!productInCart) {
    return cart.map((cartItem: CartItem) => {
      if (cartItem.productName === productName) {
        return {
          productName: cartItem.productName,
          quantity: cartItem.quantity + 1,
        } as CartItem;
      } else {
        return cartItem;
      }
    });
  } else {
    // ...otherwise add product to cart.
    return [
      ...cart,
      {
        productName: productName,
        quantity: 1,
      }
    ]
  }
}

function doRemoveProduct(allProducts: Product[], cart: CartItem[], productName: string ): CartItem[] {

  // Decrement quantity, then filter out any item where quantity less than one.
  return cart.map((cartItem: CartItem) => {
    if (cartItem.productName === productName) {
      return {
        productName: cartItem.productName,
        quantity: cartItem.quantity - 1,
      } as CartItem;
    } else {
      return cartItem;
    }
  }).filter((cartItem: CartItem) => cartItem.quantity > 0);
}


