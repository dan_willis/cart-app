import { CartItem, Product } from '../models/Product';

export interface StoreState {
  productList: Product[];
  cart: CartItem[];
}
