import * as constants from '../constants';

export interface AddProductToCart {
  type: constants.ADD_PRODUCT_TO_CART;
  payload: string; // product name
}

export interface RemoveProductFromCart {
  type: constants.REMOVE_PRODUCT_FROM_CART;
  payload: string; // product name
}

export type CartAction = AddProductToCart | RemoveProductFromCart;

export function addProductToCart(productName: string): AddProductToCart {
  return {
    type: constants.ADD_PRODUCT_TO_CART,
    payload: productName
  }
}

export function removeProductFromCart(productName: string): RemoveProductFromCart {
  return {
    type: constants.REMOVE_PRODUCT_FROM_CART,
    payload: productName
  }
}
