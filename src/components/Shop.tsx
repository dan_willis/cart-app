import React from 'react';
import { CartItem, Product } from '../models/Product';
import Cart from '../containers/Cart';
import { formatNumberAsCurrency } from '../helpers/shopHelpers';
import Paper from '@material-ui/core/Paper';
import Typography from '@material-ui/core/Typography';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemSecondaryAction from '@material-ui/core/ListItemSecondaryAction';
import ListItemText from '@material-ui/core/ListItemText';
import IconButton from '@material-ui/core/IconButton';
import AddShoppingCart from '@material-ui/icons/AddShoppingCart';
import './Shop.css';

export interface Props {
  productList: Product[];
  cart: CartItem[];
  onAddProductToCart: (productName: string) => void;
}

function Shop({ productList, cart, onAddProductToCart }: Props) {
  return (
    <div>
      <Paper className="product-list">
        <Typography className="product-list-heading" component="h1" variant="display1" gutterBottom>
          Product List
        </Typography>
        <List>
          {productList.map((product: Product) => <ListItem key={product.name}>
            <ListItemText
              primary={product.name}
              secondary={formatNumberAsCurrency(product.price)}
            />
            <ListItemSecondaryAction>
              <IconButton aria-label="Add to cart" onClick={() => onAddProductToCart(product.name)}>
                <AddShoppingCart />
              </IconButton>
            </ListItemSecondaryAction>
          </ListItem> )}
        </List>
      </Paper>
      <Paper className="cart-list">
        <Typography className="product-list-heading" component="h1" variant="display1" gutterBottom>
          Shopping cart
        </Typography>
        { !!cart.length &&
          <Cart/>
        }
        { !cart.length &&
          <p className="no-products-message">You have no products in your cart.</p>
        }

      </Paper>

    </div>
  );
}

export default Shop;
