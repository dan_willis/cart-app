import React from 'react';
import { CartItem, Product } from '../models/Product';
import { calculateCartTotal, cartItemTotalString, getPriceOfCartItem } from '../helpers/shopHelpers';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import RemoveShoppingCart from '@material-ui/icons/RemoveShoppingCart';
import IconButton from '@material-ui/core/IconButton';

export interface Props {
  productList: Product[];
  cart: CartItem[];
  onRemoveProductFromCart: (productName: string) => void;
}

function Cart({ productList, cart, onRemoveProductFromCart }: Props) {
  return (
      <Table>
        <TableHead>
          <TableRow>
            <TableCell align="right">Product</TableCell>
            <TableCell align="right">Price</TableCell>
            <TableCell align="right">Quantity</TableCell>
            <TableCell align="right">Total</TableCell>
            <TableCell></TableCell>
          </TableRow>
        </TableHead>
        <TableBody>
          {cart.map((cartItem: CartItem) => (
            <TableRow key={cartItem.productName}>
              <TableCell component="th" scope="row">{cartItem.productName}</TableCell>
              <TableCell align="right">{getPriceOfCartItem(cartItem, productList)}</TableCell>
              <TableCell align="right">{cartItem.quantity}</TableCell>
              <TableCell align="right">{cartItemTotalString(cartItem, productList)}</TableCell>
              <TableCell align="right">
                <IconButton aria-label="Add to cart" onClick={() => onRemoveProductFromCart(cartItem.productName)}>
                  <RemoveShoppingCart />
                </IconButton>
              </TableCell>
            </TableRow>
          ))}
          <TableRow>
            <TableCell component="th" scope="row" colSpan={3} align="right">Total: </TableCell>
            <TableCell><strong>{calculateCartTotal(cart, productList)}</strong></TableCell>
            <TableCell></TableCell>
          </TableRow>
        </TableBody>
      </Table>
  );
}

export default Cart;
