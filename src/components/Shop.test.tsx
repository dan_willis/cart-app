import React from 'react';
import Enzyme from 'enzyme';
import Shop from './Shop'
import { initialProducts } from '../reducers';
import { expect } from 'jest';
import List from '@material-ui/core/List';

it('shows the expected number of products in the product list', () => {
  const shop = Enzyme.shallow(<Shop productList={initialProducts} cart={[]} onAddProductToCart={() => {}} /> );
  expect(shop.find(List).children()).to.have.lengthOf(initialProducts.length)
});
