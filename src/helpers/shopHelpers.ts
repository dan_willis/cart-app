import { CartItem, Product } from '../models/Product';

function calculateCartItemTotal(cartItem: CartItem, productList: Product[]): number {
  const productMatch = productList.find((product: Product) => product.name === cartItem.productName);
  const productPrice = productMatch!.price || 0;

  return (productPrice * cartItem.quantity);
}

export function cartItemTotalString(cartItem: CartItem, productList: Product[]): string {
  return formatNumberAsCurrency(calculateCartItemTotal(cartItem, productList));
}

export function calculateCartTotal(cart: CartItem[], productList: Product[]): string {
  const cartTotal = cart
    .map((cartItem: CartItem) => calculateCartItemTotal(cartItem, productList))
    .reduce((acc, val: number) => acc + val, 0);

  return formatNumberAsCurrency(cartTotal);
}

export function formatNumberAsCurrency(price: number) {
  return price.toLocaleString('en-NZ', { style: 'currency', currency: 'NZD' });
}

export function getPriceOfCartItem(cartItem: CartItem, productList: Product[]): string {
  const productMatch = productList.find((product: Product) => product.name === cartItem.productName);
  const productPrice = productMatch!.price || 0;
  return formatNumberAsCurrency(productPrice);
}
