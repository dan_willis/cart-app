import { createStore } from 'redux';
import { cartReducer } from './reducers';
import { loadState, saveState } from './helpers/localStorage';
import { StoreState } from './types';
import { CartAction } from './actions';

export default function configureStore() {
  const persistedState = loadState();
  const store = createStore<StoreState, CartAction, any, any>(
    cartReducer,
    persistedState
  );

  store.subscribe(() => {
    saveState({
      productList: store.getState().productList,
      cart: store.getState().cart
    });
  });

  return store;
}
