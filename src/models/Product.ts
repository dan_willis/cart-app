export interface Product {
  name: string;
  price: number;
}

export interface CartItem {
  productName: string;
  quantity: number;
}
