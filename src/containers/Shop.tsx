import { StoreState } from '../types';
import { Dispatch } from 'redux';
import * as actions from '../actions';
import { connect } from 'react-redux';
import Shop from '../components/Shop';

export function mapStateToProps({ productList, cart}: StoreState) {
  return {
    productList,
    cart,
  }
}

const mapDispatchToProps = (dispatch: Dispatch<actions.CartAction>) => {
  return {
    onAddProductToCart: (productName: string) => dispatch(actions.addProductToCart(productName)),
  }
};

export default connect(mapStateToProps, mapDispatchToProps)(Shop);

