import { StoreState } from '../types';
import { Dispatch } from 'redux';
import * as actions from '../actions';
import { connect } from 'react-redux';
import Cart from '../components/Cart';

export function mapStateToProps({ productList, cart}: StoreState) {
  return {
    productList,
    cart,
  }
}

const mapDispatchToProps = (dispatch: Dispatch<actions.CartAction>) => {
  return {
    onRemoveProductFromCart: (productName: string) => dispatch(actions.removeProductFromCart(productName)),
  }
};

export default connect(mapStateToProps, mapDispatchToProps)(Cart);

