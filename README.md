
Test version: `VR-FrontendTest-v1.1`


### Instructions
To get started:

- clone this repo
- `cd` to `cart-app`
- `yarn install`

To start local dev instance:

- `yarn start`
- open [http://localhost:3000](http://localhost:3000) in a browser.

##### Notes:

- Not configured for prod builds/deployment.
- I couldn't get tests up and running. :(
- I don't feel 100% happy about calculating the cart pricing on-the-fly using helper functions in the component. I would want to find a way to have those values in the store, and to have the calculations triggered by actions.

##### Assumptions
- In lieu of unique product id, will use product name and assume it is unique.
- Cart will not store product price, but will calculate based on product price as at time of rendering.

##### State persistence 

- Persist cart state to `localStorage`.
- Methods to save and load cart state.
- Relies on State being serializable, which is good practice anyway.

#### Plan:

**2 Components:**

- Shop component
    - Lists products [product name, price, link to add product]
    - Action to add product to cart 
- Cart Component
    - Lists cart [product name, price, quantity, total, remove link.]
    - Action to remove product from cart.
    


#### References
- [https://github.com/Microsoft/TypeScript-React-Starter](https://github.com/Microsoft/TypeScript-React-Starter)

- [https://medium.com/backticks-tildes/setting-up-a-redux-project-with-create-react-app-e363ab2329b8](https://medium.com/backticks-tildes/setting-up-a-redux-project-with-create-react-app-e363ab2329b8)

- [https://medium.com/@jrcreencia/persisting-redux-state-to-local-storage-f81eb0b90e7e](https://medium.com/@jrcreencia/persisting-redux-state-to-local-storage-f81eb0b90e7e)




____________

##### Boilerplate follows:


This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## Available Scripts

In the project directory, you can run:

### `npm start`

Runs the app in the development mode.<br>
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.<br>
You will also see any lint errors in the console.

### `npm test`

Launches the test runner in the interactive watch mode.<br>
See the section about [running tests](https://facebook.github.io/create-react-app/docs/running-tests) for more information.

### `npm run build`

Builds the app for production to the `build` folder.<br>
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.<br>
Your app is ready to be deployed!

See the section about [deployment](https://facebook.github.io/create-react-app/docs/deployment) for more information.

### `npm run eject`

**Note: this is a one-way operation. Once you `eject`, you can’t go back!**

If you aren’t satisfied with the build tool and configuration choices, you can `eject` at any time. This command will remove the single build dependency from your project.

Instead, it will copy all the configuration files and the transitive dependencies (Webpack, Babel, ESLint, etc) right into your project so you have full control over them. All of the commands except `eject` will still work, but they will point to the copied scripts so you can tweak them. At this point you’re on your own.

You don’t have to ever use `eject`. The curated feature set is suitable for small and middle deployments, and you shouldn’t feel obligated to use this feature. However we understand that this tool wouldn’t be useful if you couldn’t customize it when you are ready for it.

## Learn More

You can learn more in the [Create React App documentation](https://facebook.github.io/create-react-app/docs/getting-started).

To learn React, check out the [React documentation](https://reactjs.org/).
